import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestQuadraticEquation {
    @Test
    public void testRoots(){
        QuadraticEquation q1 = new QuadraticEquation(1, -5, 4);
        QuadraticEquation q2 = new QuadraticEquation(1, -13, 12);
        QuadraticEquation q3 = new QuadraticEquation(1, -7, 12);

        List<Double> res1 = new ArrayList<>();
        List<Double> res2 = new ArrayList<>();
        List<Double> res3 = new ArrayList<>();

        Collections.addAll(res1, 4.0, 1.0);
        Collections.addAll(res2, 12.0, 1.0);
        Collections.addAll(res3, 4.0, 3.0);

        Assert.assertEquals(res1, q1.roots());
        Assert.assertEquals(res2, q2.roots());
        Assert.assertEquals(res3, q3.roots());
    }
}

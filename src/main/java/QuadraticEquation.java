import java.util.ArrayList;
import java.util.List;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public List<Double> roots(){
        List<Double> res = new ArrayList<>();
        if(a == 0){
            return res;//Не квадратное уравнение
        }
        else{
            double d = b*b - 4*a*c;
            if(d > 0){
                double x1 = (-b + Math.sqrt(d))/(2*a);
                double x2 = (-b - Math.sqrt(d))/(2*a);
                res.add(x1);
                res.add(x2);
            }
            else if(d == 0){
                double x = (-b)/(2*a);
                res.add(x);
            }
        }
        return res;
    }
}
